<?php

function getPrices()
{
    $sourceUrl = 'http://www.finansbank.enpara.com/doviz-kur-bilgileri/doviz-altin-kurlari.aspx';

    $content = file_get_contents($sourceUrl);

    $matches = array();

    $pattern = '#<div class=dlContspan>USD(.*?)<\/div><\/dd><\/dl><\/span><span><dl><dt>#is';

    preg_match_all($pattern, $content, $matches);


    if (!isset($matches[1][0])) {
        exit;
    }
    $content = $matches[1][0];

    $content = str_replace(',', '.', $content);

    $pattern = '#<span>(.*?) TL </span>#is';
    preg_match_all($pattern, $content, $matches);

    if (!isset($matches[1][0])) {
        exit;
    }

    if (!isset($matches[1][1])) {
        exit;
    }

    return array((float)$matches[1][0], (float)$matches[1][1]);
}
