<?php
include_once 'get-prices.php';

$p = getPrices();

header('Content-Type: application/json');

echo json_encode($p);
