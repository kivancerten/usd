<?php
date_default_timezone_set('Europe/Istanbul');

include_once 'get-prices.php';
include_once 'mongoSource.php';


chdir(__DIR__);

$date = date(DATE_ATOM);
$pubDate = date(DATE_RFC2822);

$guid = md5($date);

$prices = getPrices();


$XML =<<<XML
<?xml version="1.0" encoding="utf-8"?>
<feed xmlns="http://www.w3.org/2005/Atom">
    <title>USD</title>
    <updated>{$date}</updated>
      <author>
        <name>USD</name>
    </author>
    <id>urn:uuid:60a76c80-d399-11d9-b93C-0003939e0af6</id>
    <entry>
        <title>$prices[0] $prices[1]</title>
        <link href="http://kivancerten.com/#{$guid}"/>
        <id>http://kivancerten.com/#{$guid}</id>
        <updated>{$date}</updated>
    </entry>
</feed>
XML;

$mails = array('kivancerten@yahoo.com', 'gokselunal@gmail.com');

$mongoSource = new \MongoSource\MongoSource();
$mongoSource->add($prices[0], $prices[1]);

if ($prices[0] > 2.949 || $prices[1] < 2.939) {

    foreach ($mails as $mail) {
        mail($mail, 'USD ' . $prices[0] . ' ' . $prices[1], 'usd service');
    }

    file_put_contents('feed.xml', $XML);
}
