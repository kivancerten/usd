<?php
namespace MongoSource;

class MongoSource
{
    /** @var \MongoClient */
    private $mongoClient = null;

    private function initClient()
    {
        if ($this->mongoClient == null) {
            $this->mongoClient = new \MongoClient();
        }
    }

    /**
     * @return \MongoCollection
     */
    private function getCollection()
    {
        $this->initClient();

        return $this->mongoClient->selectCollection('usd', 'finans');
    }

    public function add($price1, $price2)
    {
        $doc = ['price1' => $price1, 'price2' => $price2, 'date' => new \MongoDate()];

        $response = $this->getCollection()->insert($doc);
    }

    /**
     * @return \MongoCursor
     */
    public function get() {
        return $this->getCollection()->find(['date' => ['$gt' => new \MongoDate(strtotime('-1 day'))]]);
    }

}
