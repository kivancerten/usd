<?php

date_default_timezone_set('Europe/Istanbul');

include_once 'mongoSource.php';

$mongoSource = new \MongoSource\MongoSource();

/** @var MongoCursor $cursor */
$cursor = $mongoSource->get();

$jsDataArray = array();

foreach ($cursor as $doc) {

    /** @var MongoDate $date */
    $date = $doc['date'];

    $date->sec;


    $jsDataArray[] = sprintf(
        '[new Date("%s"), %f,  %f]',
        date('D M d Y H:i:s O', $date->sec),
        $doc['price1'],
        $doc['price2']
    );
}

?>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<div id="chart_div" style="height:700px;width:auto;"></div>
<script type="text/javascript">

    google.load('visualization', '1', {packages: ['corechart', 'line']});
    google.setOnLoadCallback(drawCurveTypes);

    function drawCurveTypes() {
        var data = new google.visualization.DataTable();
        data.addColumn('datetime', 'X');
        data.addColumn('number', 'Price1');
        data.addColumn('number', 'Price2');

        data.addRows([
            <?php echo implode(',', $jsDataArray); ?>
        ]);

        var options = {
            hAxis: {
                title: 'Time'
            },
            vAxis: {
                title: 'Price'
            },
        };

        var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
        chart.draw(data, options);
    }
</script>