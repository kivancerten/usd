<?php
include_once 'get-prices.php';

$p = getPrices();

header('Content-Type: image/png');

$im = imagecreatetruecolor(200, 30);

$white = imagecolorallocate($im, 255, 255, 255);
$grey = imagecolorallocate($im, 128, 128, 128);
$black = imagecolorallocate($im, 0, 0, 0);
imagefilledrectangle($im, 0, 0, 199, 29, $white);

$text = 'USD ' . $p[0] . ' ' . $p[1];
$font = __DIR__. '/arimo.ttf';

imagettftext($im, 10, 0, 1, 11, $grey, $font, $text);
imagettftext($im, 10, 0, 0, 10, $black, $font, $text);

imagepng($im);
imagedestroy($im);
